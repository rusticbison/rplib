// daemon for managing property objects

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "lmdb++.h"
#include "lmdb_interface.h" // defines lmdb_interface
#include "command_server.h"

using namespace std;

int main()
{
    cout << "                                          \n"
            " _____ _            _          _          \n"
            "| ____| | ___  __ _| |    __ _| |__  ___  \n"
            "|  _| | |/ _ \\/ _` | |   / _` | '_ \\/ __| \n"
            "| |___| |  __/ (_| | |__| (_| | |_) \\__ \\ \n"
            "|_____|_|\\___|\\__,_|_____\\__,_|_.__/|___/ \n"
            "                                          \n"
            "MIT License                            \n"
            "Copyright 2018 Elea Labs \n"
            "Original concept by Justin Smith          \n"
            "\n"
            "For a list of available commands, type \"help\"\n\n";

    /* Create and open the LMDB environment: */
    // auto env = lmdb::env::create();
    // env.set_mapsize(1UL * 1024UL * 1024UL * 1024UL); /* 1 GiB */
    // env.open("./example.mdb", 0, 0664);
    // TODO: get this wrapper example working
    // TODO: get LMDB working

    string command;
    int termvar(0);

    while (termvar == 0)
    {

        std::cout << "|||| ";

        if (!std::getline(std::cin, command))
        { /* I/O error! */
            return -1;
        }

        if (command == "help")
        {
            list_commands();
            // termvar++;
        }

                if (command == "exit")
        {
            termvar++;
            system("clear");
        }
    }

    // char choice;

    // cout << "| ";
    // cin >> choice;
    // switch (choice)
    // {
    // case 'A':
    //     list_commands();
    //     break;
    // case 'B':
    //     cout << "You entered B.\n";
    //     break;
    // case 'C':
    //     cout << "You entered C.\n";
    //     break;
    // default:
    //     cout << "You did not enter A, B, or C!\n";
    // }
    // return 0;

    // if (!command.empty())
    // {
    //     std::cout << "Thank you, your identifier is '" << command << "'. Information about this object will be returned." << std::endl;
    //     ++score;
    // }

    // else
    // {
    //     std::cout << "Identifier failure. \n" << std::endl;
    //     --score;
    // }
}