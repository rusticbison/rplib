#include <string>
#include <iostream>

using namespace std;

void list_commands(){
    cout << "\n"
            "define object \n"
            "recall object \n"
            "list contracts \n"
            "add new contract \n"
            "revoke existing contract \n"
            "attest signature \n"
            "revoke attestation \n"
            "display recovery seed \n"
            "sell object \n"
            "buy object \n"
            "define user privileges \n"
            "\n";
}