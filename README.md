# rplib

rplib is a low level toolkit for real property operations, including validating regional notary commissions and managing legal contracts. Please note that these tools are legally untested and unproven!

## Resources

IPFS is used to publish contracts. 
opentimestamps.org is used to prove that a signed contract existed at a certain time.  

## Components

* Notary commission lookup
* Notary db (because government operated dbs are often down)
* Key lookup utility
* Key signing utility
* Key db
* Contract creation 
* Contract publication
* Contract timestamping
* Contract db

## Contributing

[Unix Philosophy](https://homepage.cs.uri.edu/~thenry/resources/unix_art/ch01s06.html): All I/O should be JSON format text, please avoid including unnecessary packages and dependencies. Keep the code simple and readable. Regional notary commissions must be added manually, so please add yours! This is the only way the notary network can grow. 

## Resources

https://jsmith.website/transacting.pdf

https://jsmith.website/transacting-implementation.html